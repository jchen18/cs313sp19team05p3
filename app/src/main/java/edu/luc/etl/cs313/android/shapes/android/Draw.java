package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;
import java.util.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStrokeColor(final StrokeColor c) {
		final int prevColor;
		prevColor= paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(prevColor);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(Style.STROKE);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		List<? extends Shape> shapes = g.getShapes();

		for (Shape shape : shapes) {
			shape.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(), -l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {

		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(Style.STROKE);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		// Get List of Points.
		final List<? extends Point> shapes = s.getPoints();
		// Initialize an array for locations the size of shapes times 4
		final float[] pts = new float[shapes.size() * 4];
		// For each point...
		int i = 0;
		for (Point point : shapes) {
			pts[i] = point.getX(); // Insert its x location.
			pts[i + 1] = point.getY(); // Insert its y location.
			i = i + 2; // Increment by 2.
		}
		canvas.drawLines(pts, paint);

		return null;
	}
}