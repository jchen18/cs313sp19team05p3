package edu.luc.etl.cs313.android.shapes.model;

import java.util.*;

/**
 * A visitor to compute the number of basic shapes in a (possibly complex)
 * shape.
 */
public class Count implements Visitor<Integer> {

	@Override
	public Integer onPolygon(final Polygon p) { return 1; }

	@Override
	public Integer onCircle(final Circle c) { return 1; }

	@Override
	public Integer onGroup(final Group g) {
		List<? extends Shape> shapes = g.getShapes();

		int total = 0;

		for (Shape shape : shapes) {
			total += shape.accept(this);
		}
		return total;
	}

	@Override
	public Integer onRectangle(final Rectangle q) {
		return 1;
	}

	@Override
	public Integer onOutline(final Outline o) {
		Shape shape = o.getShape();
		return shape.accept(this);
	}

	@Override
	public Integer onFill(final Fill c) {
		Shape shape = c.getShape();
		return shape.accept(this);
	}

	@Override
	public Integer onLocation(final Location l) {
		Shape shape = l.getShape();
		return shape.accept(this);
	}

	@Override
	public Integer onStrokeColor(final StrokeColor c) {
		Shape shape = c.getShape();
		return shape.accept(this);
	}
}
