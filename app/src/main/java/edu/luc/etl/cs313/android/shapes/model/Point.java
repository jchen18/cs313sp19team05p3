package edu.luc.etl.cs313.android.shapes.model;

/**
 * A point, implemented as a location without a shape.
 */
public class Point extends Location {

	// HINT: use a circle with radius 0 as the shape!

	protected final int x, y;

	protected final Shape shape;

	public Point(final int x, final int y) {

		super(x, y, new Circle(0)); // Create a new Location object

		// Use super calls to get this objects instance variables
		this.x = super.getX();
		this.y = super.getY();
		this.shape = super.getShape();

	}
}
