package edu.luc.etl.cs313.android.shapes.model;
import java.util.*;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		Shape shape = f.getShape();
		return shape.accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		List<? extends Shape> shapes = g.getShapes(); // Circle at 200 radius 50 is first Location
		Location location1 = shapes.get(0).accept(this);
		Location box1 = location1.accept(this);
		Rectangle rectangle1 = (Rectangle) box1.getShape();

		int left = box1.getX();
		int right = left + rectangle1.getWidth();
		int top = box1.getY();
		int bottom = top + rectangle1.getHeight();

		for(Shape shape : shapes) {
			Location location = shape.accept(this);
			Location box = location.accept(this);
			Rectangle rectangle = (Rectangle) box.getShape();

			int shapeLeft = box.getX();
			int shapeRight = shapeLeft + rectangle.getWidth();
			int shapeTop = box.getY();
			int shapeBottom = shapeTop + rectangle.getHeight();

			if (shapeLeft < left) { left = shapeLeft; }
			else if (shapeRight > right) { right = shapeRight; }
			if (shapeTop < top) { top = shapeTop; }
			else if (shapeBottom > bottom) { bottom = shapeBottom; }
		}

		int width = right - left;
		int height = bottom - top;

		return new Location(left, top, new Rectangle(width, height));
	}

	@Override
	public Location onLocation(final Location l) {
        Shape shape = l.getShape();
        Location location = shape.accept(this); // (Location) Bounding box is returned.
        Rectangle box = (Rectangle) location.getShape(); // Shape of bounding box is Rectangle.

		int x = location.getX() + l.getX();
		int y = location.getY() + l.getY();

        return new Location(x, y, box);
	}

	@Override
	public Location onRectangle(final Rectangle r) {
	    final int width = r.getWidth();
	    final int height = r.getHeight();
	    return new Location(0, 0, new Rectangle ( width, height));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		Shape shape = c.getShape();
		return shape.accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		Shape shape = o.getShape();
		return shape.accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		List<? extends Point> points = s.getPoints();

		int left = points.get(0).getX();
		int right = left;
		int top = points.get(0).getY();
		int bottom = top;

		for (Point point : points) {
			if (point.getX() < left) { left = point.getX(); }
			else if (point.getX() > right) { right = point.getX(); }
			if (point.getY() < top) { top = point.getY(); }
			else if (point.getY() > bottom){ bottom = point.getY(); }
		}

		int width = right - left;
		int height = bottom - top;

		return new Location(left, top, new Rectangle(width, height));
	}
}
